// count the total number of items supplied by Red Farms Inc.
db.fruits.aggregate([
    {$match:{supplier:"Red Farms Inc."}},
    {$count:"redFarmSupplyItems"}

])
//  count the total number of items with price greater than 50
db.fruits.aggregate([
    {$match:{price:{$gt:50}}},
    {$count:"priceGreaterThan50"}

])
//  get the average price of all fruits that are onSale per supplier
db.fruits.aggregate([
    {$match:{onSale:true}},
    {$group:{_id:"$supplier",averagePriceOnSale:{$avg:"$price"}}}
])
//  get the highest price of fruits that are onSale per supplier
db.fruits.aggregate([
     {$match:{onSale:true}},
     {$group:{_id:"$supplier",highestPriceOfFruits:{$max:"$price"}}}
])
//  get the lowerst price of fruits that are onSale per supplier
db.fruits.aggregate([
     {$match:{onSale:true}},
     {$group:{_id:"$supplier",lowestPriceOfFruits:{$min:"$price"}}}
])    